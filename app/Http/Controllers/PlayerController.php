<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Player;

class PlayerController extends Controller
{
	// show all players
	public function index(Request $request)
	{
		$players = Player::all();
		return view('players.index', compact('players'));
	}

	// show a player
	public function show(Request $request, Player $player)
	{
		return view('players.show');
	}

	// edit a player (show the edit form)
	public function edit(Request $request, Player $player)
	{
		return 'edit ' . $player;
	}

	// update a player (handle the edit form)
	public function update(Request $request, $player)
	{
		return 'update ' . $player;
	}

	// destroy a player
    public function destroy(Request $request, $player) 
    {
    	return 'destroy';
    }
}
