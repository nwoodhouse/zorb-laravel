<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function add(Request $request)
    {
    	$x = $request->input('x');
    	$y = $request->input('y');

    	return $x + $y;
    }
}
