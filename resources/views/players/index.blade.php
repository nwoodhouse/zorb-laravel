@extends('master')

@section('content')
	<h2>
		All players <span class="badge badge-success">{{ $players->count() }} total</span>
	</h2>

	@foreach($players as $player)
		<h4>
			Name: {{ $player->name }} is {{ $player->age }} years old
		</h4>
	@endforeach

@endsection