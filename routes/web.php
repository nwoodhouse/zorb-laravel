<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

// Player actions!
// CRUD

// index of all players
Route::get('/players', 'PlayerController@index');

// show a specific player
// {} indicates a route paramater - see: https://laravel.com/docs/6.x/routing#route-parameters
Route::get('/players/{player}', 'PlayerController@show');

// edit route for a player - show edit form
Route::get('/players/{player}/edit', 'PlayerController@edit');

// update route - handling the submission from the edit form
Route::put('/players/{player}', 'PlayerController@update');

// destroy route - remove the model from the db
Route::delete('/players/{player}', 'PlayerController@destroy');

// wires up all crud operations in one go!
Route::resource('teams', TeamController::class);









